
##############################
# Country definition for U01 #
##############################

country =
{ tag                 = U01
  # Resource Reserves
  energy              = 1000
  metal               = 1000
  rare_materials      = 500
  oil                 = 500
  supplies            = 500
  money               = 10
  manpower            = 20
  capital             = 1126
  diplomacy           = { }

  nationalprovinces   = { 1099 1104 1126 1128 1129
                        }
  ownedprovinces      = { 1099 1104 1126 1128 1129
                        }
  controlledprovinces = { 1099 1104 1126 1128 1129
                        }
  techapps            = {
					1980
					1260 1900
					6910
					6010 6020
					6600 6610
					6100 6110 6120 6160
                        }
  policy =
  { date              = { year = 0 month = march day = 0 }
    democratic        = 6
    political_left    = 2
    free_market       = 7
    freedom           = 1
    professional_army = 1
    defense_lobby     = 1
    interventionism   = 8
  }
}
