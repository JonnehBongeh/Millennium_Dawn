#########################################################
#							#
# Events for Southern Sudan (U24) by Zokan              #
#      			                                #
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#98501 - 98800# Main events
#98801 - 98950# Political events
#98951 - 99000# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Independence
#############################################
event = {
         id = 98501
         random = no
         country = U24
 
         name = "EVT_98501_NAME"
         desc = "EVT_98501_DESC"
         style = 0
	 picture = "sudan_cw"
 
           action_a = {
                  name = "Excellent"
                  command = { type = dissent value = -1 }
		  command = { type = war which = SUD }

		  command = { type = set_domestic which = democratic value = 7 }
		  command = { type = set_domestic which = interventionism value = 4 }
		  command = { type = set_domestic which = political_left value = 7 }
		  command = { type = set_domestic which = free_market value = 6 }
		  command = { type = set_domestic which = defense_lobby value = 4 }
		  command = { type = set_domestic which = freedom value = 6 }

		  command = { type = headofstate which = 98511 } #Garang
		  command = { type = headofgovernment which = 98512 } #Mayardit
           }

}
#############################################
###### Independence
#############################################
event = {
         id = 98502
         random = no
         country = U24
 
         name = "EVT_98502_NAME"
         desc = "EVT_98502_DESC"
         style = 0
	 picture = "sudan_cw"
 
           action_a = {
                  name = "Excellent"
                  command = { type = dissent value = -1 }

		  command = { type = set_domestic which = democratic value = 7 }
		  command = { type = set_domestic which = interventionism value = 3 }
		  command = { type = set_domestic which = political_left value = 7 }
		  command = { type = set_domestic which = free_market value = 6 }
		  command = { type = set_domestic which = defense_lobby value = 3 }
		  command = { type = set_domestic which = freedom value = 6 }

		  command = { type = headofstate which = 98501 } #Mayardit
		  command = { type = headofgovernment which = 98502 } #Machar
           }

}