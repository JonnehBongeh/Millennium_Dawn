
	target_hostile_influence = {

		icon = decision
		
		visible = {
			hidden_trigger = { is_ai = no }
			NOT = { FROM = { tag = ROOT } }
		}
		
		available = {
			ROOT = { has_opinion = { target = FROM value < -5 } }
		}
		
		cost = 100
		days_remove = 60
		days_re_enable = 0
		
		complete_effect = {
			custom_effect_tooltip = target_hostile_influence_TT
			FROM = {
				add_opinion_modifier = {
					target = ROOT 
					modifier = combated_our_influence
				}
			}
			#hidden_effect = {
				if = { limit = { check_variable = { influence_array^0 = FROM } }
					subtract_from_variable = { influence_array_val^0 = 15 }
					clamp_variable = {
						var = influence_array_val^0
						min = 0
						max = influence_total
					}
					
						else = { if = { limit = { check_variable = { influence_array^1 = FROM } }
						subtract_from_variable = { influence_array_val^1 = 15 }
						clamp_variable = {
							var = influence_array_val^1
							min = 0
							max = influence_total
						}
						
							else = { if = { limit = { check_variable = { influence_array^2 = FROM } }
							subtract_from_variable = { influence_array_val^2 = 15 }
							clamp_variable = {
								var = influence_array_val^2
								min = 0
								max = influence_total
							}
							
								else = { if = { limit = { check_variable = { influence_array^3 = FROM } }
								subtract_from_variable = { influence_array_val^3 = 15 }
								clamp_variable = {
									var = influence_array_val^3
									min = 0
									max = influence_total
								}
								
									else = { if = { limit = { check_variable = { influence_array^4 = FROM } }
									subtract_from_variable = { influence_array_val^4 = 15 }
									clamp_variable = {
										var = influence_array_val^4
										min = 0
										max = influence_total
									}
									
										else = { if = { limit = { check_variable = { influence_array^5 = FROM } }
										subtract_from_variable = { influence_array_val^5 = 15 }
										clamp_variable = {
											var = influence_array_val^5
											min = 0
											max = influence_total
										}
										
											else = { if = { limit = { check_variable = { influence_array^6 = FROM } }
											subtract_from_variable = { influence_array_val^6 = 15 }
											clamp_variable = {
												var = influence_array_val^6
												min = 0
												max = influence_total
											}
										} }
									} }
								} }
							} }
						} }
					} }
				}
			#}
			recalculate_influence = yes
		}
	}