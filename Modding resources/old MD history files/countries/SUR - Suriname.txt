﻿capital = 309

oob = "SUR_2000"

set_convoys = 230
set_stability = 0.5

set_country_flag = country_language_dutch

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	limited_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 30
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 25
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = social_liberal
	last_election = "1996.5.23"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Ronald Venetiaan"
	ideology = centrist
	picture = "Ronald_Venetiaan.dds"
}

create_field_marshal = {
	name = "Adolf Jardim"
	picture = "Portrait_Adolf_Jardim.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_field_marshal = {
	name = "Robert Kartodikromo"
	picture = "Portrait_Robert_Kartodikromo.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_corps_commander = {
	name = "Egmond Letterboom"
	picture = "Portrait_Egmond_Letterboom.dds"
	traits = { trait_engineer trickster }
	skill = 1
}

create_corps_commander = {
	name = "Henri van Axeldongen"
	picture = "Portrait_Henri_van_Axeldongen.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Maipath Ganpat"
	picture = "Portrait_Maipath_Ganpat.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_navy_leader = {
	name = "Marino Acton"
	picture = "Portrait_Marino_Acton.dds"
	traits = { blockade_runner }
	skill = 1
}

2015.1.1 = {
	create_country_leader = {
		name = "Dési Bouterse"
		ideology = liberalist
		picture = "Desi_Bouterse.dds"
	}
}