﻿capital = 505

oob = "BRA_2000"

set_convoys = 40
set_stability = 0.5

set_country_flag = country_language_portuguese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	APC_3 = 1
	Air_APC_3 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	IFV_3 = 1
	Air_IFV_3 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1


	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {
	parties = {
		islamist = {
			popularity = 0
		}
		fascist = {
			popularity = 1
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 1
		}
		conservative = {
			popularity = 20
		}
		market_liberal = {
			popularity = 11
		}
		social_liberal = {
			popularity = 16
		}
		social_democrat = {
			popularity = 30
		}
		progressive = {
			popularity = 1
		}
		democratic_socialist = {
			popularity = 18
		}
		communist = {
			popularity = 2
		}
	}
	ruling_party = social_democrat
	last_election = "1998.10.4"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Fernando H. Cardoso"
	picture = "Fernando_Cardoso.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Luiz Inacio Lula da Silva"
	picture = "Luiz_Inacio_Lula_da_Silva.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Eneas Carneiro"
	picture = "Eneas_Carneiro.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Alfredo Syrkis"
	picture = "Alfredo_Syrkis.dds"
	ideology = green
}

create_country_leader = {
	name = "Marco Maciel"
	picture = "Marco_Maciel.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Jader Barbalho"
	picture = "Jader_Barbalho.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Luiz of Orleans-Braganza"
	picture = "Luiz.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Levy Fidelix"
	picture = "Levy_Fidelix.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Renato Rabelo"
	picture = "Renato_Rabelo.dds"
	ideology = leninist
}

create_country_leader = {
	name = "Valdemar Costa Neto"
	picture = "Valdemar_Costa_Neto.dds"
	ideology = proto_fascist
}

create_country_leader = {
	name = "Ciro Nogueira"
	picture = "Ciro_Nogueira.dds"
	ideology = libertarian
}

add_namespace = {
	name = "bra_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Fernando Azevedo e Silva"
	picture = "generals/Fernando_e_Silva.dds"
	traits = { organisational_leader }
	skill = 1
}
create_field_marshal = {
	name = "Araken de Albuquerque"
	picture = "generals/Araken.dds"
	traits = { fast_planner }
	skill = 1
}
create_field_marshal = {
	name = "Manoel Luiz Narvaz Pafiadache"
	picture = "generals/Pafiadache.dds"
	traits = { thorough_planner }
	skill = 1
}
create_field_marshal = {
	name = "João Camilo Pires de Campos"
	picture = "generals/Campos.dds"
	traits = { defensive_doctrine }
	skill = 1
}
create_field_marshal = {
	name = "Juarez Aparecido de Paula Cunha"
	picture = "generals/Juarez_Aparecido.dds"
	traits = { inspirational_leader }
	skill = 1
}
create_field_marshal = {
	name = "Marco Antônio de Farias"
	picture = "generals/Marco_de_Farias.dds"
	traits = { logistics_wizard }
	skill = 1
}
create_field_marshal = {
	name = "Nivaldo Luís Rossato"
	picture = "generals/Nivaldo_Rosato.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_corps_commander = {
	name = "Joaquim Maia Brandão Júnior"
	picture = "generals/Joaquim_Brandao.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Antonio Miotto"
	picture = "generals/Antonio_Miotto.dds"
	traits = { jungle_rat swamp_fox }
	skill = 1
}
create_corps_commander = {
	name = "Artur Costa Moura"
	picture = "generals/Artur_Costa_Moura.dds"
	traits = { ranger hill_fighter }
	skill = 1
}
create_corps_commander = {
	name = "Gerson Menandro Garcia de Freitas"
	picture = "generals/Gerson_Menandro.dds"
	traits = { urban_assault_specialist panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Luiz Carlos Pereira Gomes"
	picture = "generals/Luis_Carlos_Pereira_Gomes.dds"
	traits = { swamp_fox }
	skill = 1
}
create_corps_commander = {
	name = "Mauro Cesar Lourena Cid"
	picture = "generals/Mauro_Cid.dds"
	traits = { urban_assault_specialist ranger }
	skill = 1
}
create_corps_commander = {
	name = "Cláudio Coscia Moura"
	picture = "generals/Claudio_Moura.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Valerio Stumpf Trindade"
	picture = "generals/Valerio_Stumpf.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Walmir Almada Schneider Filho"
	picture = "generals/Walmir_Schneider.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Carlos Alberto Mansur"
	picture = "generals/Carlos_Mansur.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Joarez Alves Pereira Junior"
	picture = "generals/Joarez_Alves_Pereira.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Márcio Roland Heise"
	picture = "generals/Marcio_Heise.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Luciano José Penna"
	picture = "generals/Luciano_Penna.dds"
	traits = { fortress_buster }
	skill = 1
}
create_corps_commander = {
	name = "Marco Antônio Freire Gomes"
	picture = "generals/Freire_Gomes.dds"
	traits = { fortress_buster }
	skill = 1
}
create_corps_commander = {
	name = "Antônio Carlos Machado Faillace"
	picture = "generals/Faillace.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Antônio Maxwell de Oliveira Eufrásio"
	picture = "generals/Antonio_Maxwell_de_Oliveira.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Kleber Nunes de Vasconcellos"
	picture = "generals/Kleber_de_Vasconcellos.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Sérgio Schwingel"
	picture = "generals/Sergio_Schwingel.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}
create_corps_commander = {
	name = "Achilles Furlan Neto"
	picture = "generals/Achilles_Furlan_Neto.dds"
	traits = { commando }
	skill = 1
}
create_corps_commander = {
	name = "Adilson Carlos Katibe"
	picture = "generals/Adilson_Katibe.dds"
	traits = { bearer_of_artillery }
	skill = 1
}
create_corps_commander = {
	name = "Aléssio Oliveira da Silva"
	picture = "generals/Alessio_da_Silva.dds"
	traits = { bearer_of_artillery }
	skill = 1
}
create_corps_commander = {
	name = "Wilson Mendes Lauria"
	picture = "generals/Wilson_Lauria.dds"
	traits = { jungle_rat }
	skill = 1
}
create_corps_commander = {
	name = "Hertz Pires do Nascimento"
	picture = "generals/Hertz_do_Nascimento.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Lourenço Willian da Silva Ribeiro Pinho"
	picture = "generals/Lourenco_William.dds"
	traits = { ranger hill_fighter }
	skill = 1
}
create_corps_commander = {
	name = "Jorge Roberto Lopes Fossi"
	picture = "generals/Jorge_Fossi.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Gustavo Henrique Dutra de Menezes"
	picture = "generals/Gustavo_Dutra.dds"
	traits = { hill_fighter }
	skill = 1
}
create_corps_commander = {
	name = "Fernando Telles Ferreira Bandeira"
	picture = "generals/Fernando_Telles_Ferreira.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Heber Garcia Portella"
	picture = "generals/Heber_Garcia_Portella.dds"
	traits = { ranger urban_assault_specialist }
	skill = 1
}
create_corps_commander = {
	name = "Carlos André Alcântara Leite"
	picture = "generals/Carlos_Alcantara_Leite.dds"
	traits = { trait_mountaineer }
	skill = 1
}
create_corps_commander = {
	name = "Jayme Octávio de Alexandre Queiroz"
	picture = "generals/Jayme_de_Alexandre_Queiroz.dds"
	traits = { ranger urban_assault_specialist }
	skill = 1
}
create_corps_commander = {
	name = "Carlos Alberto Dahmer"
	picture = "generals/Carlos_Alberto_Dahmer.dds"
	traits = { trait_engineer urban_assault_specialist }
	skill = 1
}
create_corps_commander = {
	name = "Francisco Humberto Montenegro Junior"
	picture = "generals/Francisco_Humberto_Montenegro.dds"
	traits = { trait_engineer urban_assault_specialist }
	skill = 1
}
create_corps_commander = {
	name = "Ricardo Rodrigues Canhaci"
	picture = "generals/Ricardo_Canahaci.dds"
	traits = { swamp_fox }
	skill = 1
}
create_corps_commander = {
	name = "Sérgio Luiz Tratz"
	picture = "generals/Sergio_Luiz_Tratz.dds"
	traits = { trait_engineer trickster }
	skill = 1
}
create_corps_commander = {
	name = "José Eduardo Leal de Oliveira"
	picture = "generals/Jose_Leal_de_Oliveira.dds"
	traits = { trickster }
	skill = 1
}
create_corps_commander = {
	name = "João Denison Maia Correia"
	picture = "generals/Joao_Maia_Correia.dds"
	traits = { hill_fighter }
	skill = 1
}
create_corps_commander = {
	name = "Daniel de Almeida Dantas"
	picture = "generals/Daniel_Dantas.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Marcelo Arantes Guedon"
	picture = "generals/Marcelo_Arantes_Guedon.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Fernando Antonio De Siqueira Ribeiro"
	picture = "generals/Fernando_Antonio_de_Siqueira_Ribeiro.dds"
	traits = { naval_invader }
	skill = 1
}
create_corps_commander = {
	name = "Jose Carlos de Nardi"
	picture = "generals/Jose_Carlos_de_Nardi.dds"
	skill = 2
}
create_corps_commander = {
	name = "Fernando Antonio de Siqueira Ribeiro"
	picture = "generals/Fernando_AdS_Ribeiro.dds"
	skill = 1
}
create_corps_commander = {
	name = "Eduardo Villas Boas"
	picture = "generals/Eduardo_V_Boas.dds"
	skill = 1
}
create_corps_commander = {
	name = "Francisco Carlos Modesto"
	picture = "generals/Francisco_C_Modesto.dds"
	skill = 1
}
create_corps_commander = {
	name = "Mauro Sinott Lopes"
	picture = "generals/Mauro_Sinott_Lopes.dds"
	skill = 1
}
create_corps_commander = {
	name = "Carlos Alberto dos Santos Cruz"
	picture = "generals/Carlos_AdS_Cruz.dds"
	skill = 1
}
create_corps_commander = {
	name = "Floriano Peixoto Vieira Neto"
	picture = "generals/Floriano_PV_Neto.dds"
	skill = 1
}
create_corps_commander = {
	name = "Enzo Martins Peri"
	picture = "generals/Enzo_Martins_Peri.dds"
	skill = 1
}
create_corps_commander = {
	name = "Edison Leal Pujol"
	picture = "generals/Edison_Leal_Pujol.dds"
	skill = 1
}

create_navy_leader = {
	name = "Eduardo Bacellar Leal Ferreira"
	picture = "admirals/Eduardo_Bacellar.dds"
	traits = { ironside }
	skill = 1
}
create_navy_leader = {
	name = "Julio Soares de Moura Neto"
	picture = "admirals/Julio_Soares_de_Moura_Neto.dds"
	traits = { blockade_runner }
	skill = 1
}
create_navy_leader = {
	name = "Victor Cardoso Gomes"
	picture = "admirals/Victor_Cardoso_Gomes.dds"
	traits = { seawolf }
	skill = 1
}
create_navy_leader = {
	name = "Glauco Castilho Dall’Antonia"
	picture = "admirals/Glauco_Castilho.dds"
	traits = { spotter }
	skill = 1
}
create_navy_leader = {
	name = "Liseo Zampronio"
	picture = "admirals/Liseo_Zampronio.dds"
	traits = { fly_swatter }
	skill = 1
}
create_navy_leader = {
	name = "Eduardo Leal Ferreira"
	picture = "admirals/Eduardo_Leal_Ferreira.dds"
	skill = 1
}
create_navy_leader = {
	name = "Ademir Sobrinho"
	picture = "admirals/Ademir_Sobrinho.dds"
	skill = 1
}

2007.1.1 = {
	set_party_name = {
		ideology = conservative
		long_name = BRA_conservative_party_DEM_long
		name = BRA_conservative_party_DEM
	}
}

2014.10.26 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 0
			}
			fascist = {
				popularity = 0
			}
			nationalist = {
				popularity = 6
			}
			reactionary = {
				popularity = 1
			}
			conservative = {
				popularity = 10
			}
			market_liberal = {
				popularity = 6
			}
			social_liberal = {
				popularity = 16
			}
			social_democrat = {
				popularity = 28
			}
			progressive = {
				popularity = 3
			}
			democratic_socialist = {
				popularity = 25
			}
			communist = {
				popularity = 5
			}
		}
	ruling_party = democratic_socialist
	last_election = "2014.10.26"
	election_frequency = 48
	elections_allowed = yes
}

	create_country_leader = {
	name = "Dilma Rousseff"
	desc = "POLITICIAN_BRA_DILMA_ROUSSEFF_DESC"
	picture = "Dilma_Rousseff.dds"
	ideology = democratic_socialist_ideology
	}

	create_country_leader = {
	name = "Aecio Neves"
	desc = "POLITICIAN_BRA_AECIO_NEVES_DESC"
	picture = "Aecio_Neves.dds"
	ideology = social_democrat_ideology
	}

	create_country_leader = {
	name = "Eduardo Jorge"
	desc = "POLITICIAN_BRA_EDUARDO_JORGE_DESC"
	picture = "Eduardo_Jorge.dds"
	ideology = green
	}

	create_country_leader = {
	name = "Mauro Lasi"
	desc = "POLITICIAN_BRA_MAURO_LASI_DESC"
	picture = "Mauro_Lasi.dds"
	ideology = leninist
	}
	
	create_country_leader = {
	name = "Jair Bolsonaro"
	picture = "Jair_Bolsonaro.dds"
	ideology = counter_progressive_democrat
	}

	create_country_leader = {
	name = "Romero Juca"
	desc = "POLITICIAN_BRA_ROMERO_JUCA_DESC"
	picture = "Romero_Juca.dds"
	ideology = moderate
	}



}

2017.1.1 = {

oob = "BRA_2017"

set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
 
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		night_vision_4 = 1
		
		#Prosub program
		submarine_1 = 1
		
		diesel_attack_submarine_1 = 1
		diesel_attack_submarine_2 = 1
		diesel_attack_submarine_3 = 1
		diesel_attack_submarine_4 = 1

		#Niterói Class
		frigate_1 = 1
		frigate_2 = 1
		
		#Barroso-Class
		corvette_1 = 1
		corvette_2 = 1
		missile_corvette_1 = 1
		missile_corvette_2 = 1
		
		
		Early_APC = 1
		
		#M41C
		#Tamoyo III
		#EE-T1 Osorio
		MBT_1 = 1
		MBT_2 = 1
		MBT_3 = 1

		#EE-9 Cascavel & X1A2
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		
		
		#EE-11 Urutu I & II
		APC_1 = 1
		APC_2 = 1
		
		#CBTP Charrua
		APC_3 = 1
		
		Air_APC_4 = 1
		APC_4 = 1
		
		#VBTP-MR Guarani
		APC_5 = 1
		
		#EE-11 Urutu I & II IFV
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		
		#VBCI-MR Guarani
		IFV_5 = 1
		
		gw_artillery = 1
		
		SP_arty_equipment_0 = 1
		SP_arty_equipment_1 = 1
		SP_arty_equipment_2 = 1
		
		Arty_upgrade_1 = 1
		Arty_upgrade_2 = 1
		Arty_upgrade_3 = 1
		
		SP_R_arty_equipment_0 = 1
		SP_R_arty_equipment_1 = 1
		SP_R_arty_equipment_2 = 1
		
		#MSS-1.2
		Anti_tank_0 = 1
		Anti_tank_1 = 1
		Anti_tank_2 = 1
		
		AT_upgrade_1 = 1
		AT_upgrade_2 = 1
		
		AA_upgrade_1 = 1
		AA_upgrade_3 = 1
		
		SP_Anti_Air_0 = 1
		SP_Anti_Air_1 = 1
		Anti_Air_0 = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		infantry_weapons4 = 1
		infantry_weapons5 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		command_control_equipment3 = 1
		command_control_equipment4 = 1
		
		combat_eng_equipment = 1
		
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1
		land_Drone_equipment2 = 1
		
		Heavy_Anti_tank_0 = 1
		Heavy_Anti_tank_1 = 1
		
		util_vehicle_equipment_0 = 1
		util_vehicle_equipment_1 = 1
		util_vehicle_equipment_2 = 1
		util_vehicle_equipment_3 = 1
		util_vehicle_equipment_4 = 1
		
		ENG_MBT_1 = 1

		early_fighter = 1
		
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1
		L_Strike_fighter3 = 1
		
		Strike_fighter1 = 1
		Strike_fighter2 = 1
		
		#KC-390
		early_bomber = 1
		transport_plane1 = 1
		transport_plane2 = 1
		transport_plane3 = 1

	landing_craft = 1
}

set_variable = { var = debt value = 1553 }
set_variable = { var = int_investments value = 7.3 }
set_variable = { var = treasury value = 381 }
set_variable = { var = tax_rate value = 35 }

add_ideas = {
	pop_050
	christian
	brazilian_knack
	#barons_legacy
	unrestrained_corruption
	gdp_4
	rio_pact_member
	industrial_conglomerates
	labour_unions
	small_medium_business_owners	#to be replaced with custom one
		recession
		defence_01
	edu_04
	health_02
	social_05
	bureau_03
	police_02
	volunteer_army
	volunteer_women
	industrial_conglomerates
	labour_unions
	farmers
	civil_law
	cartels_3
}
set_country_flag = gdp_4
set_country_flag = enthusiastic_industrial_conglomerates
set_country_flag = hostile_labour_unions
set_country_flag = enthusiastic_farmers



give_guarantee = ARG
give_guarantee = PAR
give_guarantee = URG

# complete_national_focus = lava_jato
# complete_national_focus = barons_legacy
# complete_national_focus = bilateralism_focus
# complete_national_focus = br_western_focus
# complete_national_focus = br_support_us

#Nat focus #should be added to focus tree
	complete_national_focus = BRA_tech_slots
	complete_national_focus = BRA_4K_GDPC_slot
	complete_national_focus = BRA_4_IC_slot
	complete_national_focus = BRA_8_IC_slot
	complete_national_focus = BRA_16_IC_slot
	complete_national_focus = BRA_30_IC_slot
	complete_national_focus = BRA_50_IC_slot
	set_politics = {

	parties = {
		democratic = { 
			popularity = 19
		}
		
		nationalist = {
			popularity = 30
		}
		
		communism = {
			popularity = 38
		}
		
		neutrality = { 
			popularity = 13
		}
	}
	
	ruling_party = neutrality
	last_election = "2014.10.13"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Michel Temer"
	desc = ""
	picture = "Michel_Temer.dds"
	expire = "2065.1.1"
	ideology = neutral_social
	traits = {
		lawyer
		political_dancer
		deceitful
		sly
	}
}

create_country_leader = {
	name = "Lula"
	desc = ""
	picture = "Lula.dds"
	expire = "2065.1.1"
	ideology = anarchist_communism
	traits = {
		union_man
	}
}

# create_country_leader = {
	# name = "Aécio Neves"
	# desc = ""
	# picture = "Aecio.dds"
	# expire = "2065.1.1"
	# ideology = liberalism
	# traits = {
	# }
# }

create_country_leader = {
	name = "João Dória"
	desc = ""
	picture = "Doria.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
	}
}

create_country_leader = {
	name = "Jair Bolsonaro"
	desc = ""
	picture = "Bolsonaro.dds"
	expire = "2065.1.1"
	ideology = nat_populism
	traits = {
		cornered_fox
	}
}

# create_country_leader = {
	# name = "Kleber Antonio Pivetti Busch"
	# desc = ""
	# picture = "Kleber_AP_Busch.dds"
	# expire = "2065.1.1"
	# ideology = Kingdom
	# traits = {
	# }
# }


create_corps_commander = {
	name = "Edson Leal Pujol"
	portrait_path = "gfx/leaders/BRA/General_Edson_Leal_Pujol.dds"
	traits = { ranger }
	skill = 3
}

create_corps_commander = {
	name = "Araken de Albuquerque"
	portrait_path = "gfx/leaders/BRA/General_Araken_de_Albuquerque.dds"
	traits = { jungle_rat }
	skill = 3
}

create_corps_commander = {
	name = "Francisco Carlos Modesto"
	portrait_path = "gfx/leaders/BRA/General_Francisco_Carlos_Modesto.dds"
	traits = {}
	skill = 5
}

create_corps_commander = {
	name = "João Camilo Pires de Campos"
	portrait_path = "gfx/leaders/BRA/General_Campos.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Marco Antônio de Farias"
	portrait_path = "gfx/leaders/BRA/General_Marco_Antonio_de_Farias.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Juarez Aparecido de Paula Cunha"
	portrait_path = "gfx/leaders/BRA/General_Juarez_Aparecido.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Joaquim Maia Brandão Júnior"
	portrait_path = "gfx/leaders/BRA/General_Joaquim_Brandao.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Guilherme Cals Theophilo Gaspar de Oliveira"
	portrait_path = "gfx/leaders/BRA/General_Theophilo.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Oswaldo de Jesus Ferreira"
	portrait_path = "gfx/leaders/BRA/General_Oswaldo_de_Jesus.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Manoel Luiz Narvaz Pafiadache"
	portrait_path = "gfx/leaders/BRA/General_Manoel_Luiz.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Paulo Humberto Cesar de Oliveira"
	portrait_path = "gfx/leaders/BRA/General_Paulo_Humberto.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Fernando Azevedo e Silva"
	portrait_path = "gfx/leaders/BRA/General_Fernando_Azevedo.dds"
	traits = {}
	skill = 3
}

create_corps_commander = {
	name = "Mauro Cesar Lourena Cid"
	portrait_path = "gfx/leaders/BRA/General_Cid.dds"
	traits = {}
	skill = 3
}

create_navy_leader = {
	name = "Augusto Rademaker"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_navy_3.dds"
	traits = {  }
	skill = 3
}
create_field_marshal = {
	name = "Eduardo Villas Bôas"
	picture = "Portrait_Eduardo_Villas_Boas.dds"
	traits = { old_guard offensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Fernando Azevedo e Silva"
	picture = "Portrait_Fernando_e_Silva.dds"
	traits = { organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "João Camilo Pires de Campos"
	picture = "Portrait_Campos.dds"
	traits = { defensive_doctrine }
	skill = 3
}

create_field_marshal = {
	name = "Nivaldo Luís Rossato"
	picture = "Portrait_Nivaldo_Rosato.dds"
	traits = { logistics_wizard }
	skill = 4
}

create_corps_commander = {
	name = "Antonio Miotto"
	picture = "Portrait_Antonio_Miotto.dds"
	traits = { jungle_rat swamp_fox }
	skill = 3
}

create_corps_commander = {
	name = "Artur Costa Moura"
	picture = "Portrait_Artur_Costa_Moura.dds"
	traits = { ranger hill_fighter }
	skill = 3
}

create_corps_commander = {
	name = "Gerson Menandro Garcia de Freitas"
	picture = "Portrait_Gerson_Menandro.dds"
	traits = { urban_assault_specialist panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Luiz Carlos Pereira Gomes"
	picture = "Portrait_Luis_Carlos_Pereira_Gomes.dds"
	traits = { swamp_fox }
	skill = 2
}

create_corps_commander = {
	name = "Cláudio Coscia Moura"
	picture = "Portrait_Claudio_Moura.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Valerio Stumpf Trindade"
	picture = "Portrait_Valerio_Stumpf.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Walmir Almada Schneider Filho"
	picture = "Portrait_Walmir_Schneider.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Carlos Alberto Mansur"
	picture = "Portrait_Carlos_Mansur.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Joarez Alves Pereira Junior"
	picture = "Portrait_Joarez_Alves_Pereira.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Márcio Roland Heise"
	picture = "Portrait_Marcio_Heise.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Luciano José Penna"
	picture = "Portrait_Luciano_Penna.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Marco Antônio Freire Gomes"
	picture = "Portrait_Freire_Gomes.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Antônio Carlos Machado Faillace"
	picture = "Portrait_Faillace.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Antônio Maxwell de Oliveira Eufrásio"
	picture = "Portrait_Antonio_Maxwell_de_Oliveira.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Kleber Nunes de Vasconcellos"
	picture = "Portrait_Kleber_de_Vasconcellos.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Sérgio Schwingel"
	picture = "Portrait_Sergio_Schwingel.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Mauro Sinott Lopes"
	picture = "Portrait_Mauro_Sinott_Lopes.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Achilles Furlan Neto"
	picture = "Portrait_Achilles_Furlan_Neto.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Adilson Carlos Katibe"
	picture = "Portrait_Adilson_Katibe.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Aléssio Oliveira da Silva"
	picture = "Portrait_Alessio_da_Silva.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Wilson Mendes Lauria"
	picture = "Portrait_Wilson_Lauria.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Hertz Pires do Nascimento"
	picture = "Portrait_Hertz_do_Nascimento.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Lourenço Willian da Silva Ribeiro Pinho"
	picture = "Portrait_Lourenco_William.dds"
	traits = { ranger hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Jorge Roberto Lopes Fossi"
	picture = "Portrait_Jorge_Fossi.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Henrique Dutra de Menezes"
	picture = "Portrait_Gustavo_Dutra.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Heber Garcia Portella"
	picture = "Portrait_Heber_Garcia_Portella.dds"
	traits = { ranger urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Carlos André Alcântara Leite"
	picture = "Portrait_Carlos_Alcantara_Leite.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Jayme Octávio de Alexandre Queiroz"
	picture = "Portrait_Jayme_de_Alexandre_Queiroz.dds"
	traits = { ranger urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Alberto Dahmer"
	picture = "Portrait_Carlos_Alberto_Dahmer.dds"
	traits = { trait_engineer urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Ricardo Rodrigues Canhaci"
	picture = "Portrait_Ricardo_Canahaci.dds"
	traits = { swamp_fox }
	skill = 1
}

create_corps_commander = {
	name = "Sérgio Luiz Tratz"
	picture = "Portrait_Sergio_Luiz_Tratz.dds"
	traits = { trait_engineer trickster }
	skill = 1
}

create_corps_commander = {
	name = "José Eduardo Leal de Oliveira"
	picture = "Portrait_Jose_Leal_de_Oliveira.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Daniel de Almeida Dantas"
	picture = "Portrait_Daniel_Dantas.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Marcelo Arantes Guedon"
	picture = "Portrait_Marcelo_Arantes_Guedon.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Fernando Antonio De Siqueira Ribeiro"
	picture = "Portrait_Fernando_Antonio_de_Siqueira_Ribeiro.dds"
	traits = { naval_invader }
	skill = 3
}

create_navy_leader = {
	name = "Ademir Sobrinho"
	picture = "Portrait_Ademir_Sobrinho.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Eduardo Bacellar Leal Ferreira"
	picture = "Portrait_Eduardo_Bacellar.dds"
	traits = { ironside }
	skill = 3
}

create_navy_leader = {
	name = "Julio Soares de Moura Neto"
	picture = "Portrait_Julio_Soares_de_Moura_Neto.dds"
	traits = { blockade_runner }
	skill = 3
}

create_navy_leader = {
	name = "Victor Cardoso Gomes"
	picture = "Portrait_Victor_Cardoso_Gomes.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Glauco Castilho Dall’Antonia"
	picture = "Portrait_Glauco_Castilho.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "Liseo Zampronio"
	picture = "Portrait_Liseo_Zampronio.dds"
	traits = { fly_swatter }
	skill = 1
}

}