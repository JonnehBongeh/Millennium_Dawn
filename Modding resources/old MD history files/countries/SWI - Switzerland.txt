﻿capital = 151

oob = "SWI_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_german
set_country_flag = country_language_french
set_country_flag = country_language_italian
set_country_flag = country_language_romansh

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	swiss_political_system
	neutrality_idea
	free_trade
	limited_conscription
}

give_guarantee = LIC

set_politics = {

	parties = {
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 2
		}
		reactionary = {
			popularity = 22.6
		}
		conservative = {
			popularity = 2
		}
		market_liberal = {
			popularity = 19.9
		}
		social_liberal = {
			popularity = 15.8
		}
		social_democrat = {
			popularity = 22.5
		}
		progressive = {
			popularity = 5
		}
		democratic_socialist = {
			popularity = 1
		}
		communist = {
			popularity = 0
		}
	}

	ruling_party = reactionary
	last_election = "1999.9.24"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Adolf Ogi"
	picture = "Adolf_Ogi.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Rudolf Keller"
	picture = "Rudolf_Keller.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Johann Schneider-Ammann"
	picture = "Johann_Schneider_Ammann.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Doris Leuthard"
	picture = "Doris_Leuthard.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Moritz Leuenberger"
	picture = "Moritz_Leuenberger.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Dominic Luthard"
	picture = "Dominic_Luthard.dds"
	ideology = national_socialist 
}

create_country_leader = {
	name = "Josef Zisyadis"
	picture = "Josef_Zisyadis.dds"
	ideology = democratic_socialist_ideology 
}

create_country_leader = {
	name = "Marianne Huguenin"
	picture = "Marianne_Huguenin.dds"
	ideology = marxist 
}

create_country_leader = {
	name = "Ruth Genner"
	picture = "Ruth_Genner.dds"
	ideology = green 
}

create_field_marshal = {
	name = "Philippe Rebord"
	picture = "Portrait_Philippe_Rebord.dds"
	traits = { old_guard organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "André Blattmann"
	picture = "Portrait_Andre_Blattmann.dds"
	traits = { thorough_planner }
	skill = 1
}

create_corps_commander = {
	name = "Aldo C. Schellenberg"
	picture = "Portrait_Aldo_Schellenberg.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Jean-Paul Theler"
	picture = "Portrait_Jean-Paul_Theler.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Thomas Kaiser"
	picture = "Portrait_Thomas_Kaiser.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Keller"
	picture = "Portrait_Daniel_Keller.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Willy Brülisauer"
	picture = "Portrait_Willy_Bruelisauer.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Peter Baumgartner"
	picture = "Portrait_Peter_Baumgartner.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans Schori"
	picture = "Portrait_Hans_Schori.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Laurent Michaud"
	picture = "Portrait_Laurent_Michaud.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Raynald Droz"
	picture = "Portrait_Raynald_Droz.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Alain Vuitel"
	picture = "Portrait_Alain_Vuitel.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Fredy Keller"
	picture = "Portrait_Fredy_Keller.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Hans Schatzmann"
	picture = "Portrait_Hans_Schatzmann.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Claude Meier"
	picture = "Portrait_Claude_Meier.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Franz Nager"
	picture = "Portrait_Franz_Nager.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Baumgartner"
	picture = "Portrait_Daniel_Baumgartner.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "René Wellinger"
	picture = "Portrait_Rene_Wellinger.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Yvon Langel"
	picture = "Portrait_Yvon_Langel.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Mathias Tüscher"
	picture = "Portrait_Mathias_Tuescher.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Kohli"
	picture = "Portrait_Alexander_Kohli.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Martin Vögeli"
	picture = "Portrait_Martin_Voegeli.dds"
	traits = { winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Maurizio Dattrino"
	picture = "Portrait_Maurizio_Dattrino.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Erik Labara"
	picture = "Portrait_Erik_Labara.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Roland Favre"
	picture = "Portrait_Roland_Favre.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans-Peter Walser"
	picture = "Portrait_Hans-Peter_Walser.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Lucas Caduff"
	picture = "Portrait_Lucas_Caduff.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans-Peter Kellerhals"
	picture = "Portrait_Hans-Peter_Kellerhals.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Dominique Andrey"
	picture = "Portrait_Dominique_Andrey.dds"
	traits = { trickster }
	skill = 1
}

2001.1.1 = {
	complete_national_focus = SWI_strengthen_the_government
	complete_national_focus = SWI_join_the_united_nations
	complete_national_focus = SWI_economic_independence
	complete_national_focus = SWI_join_schengen
	complete_national_focus = SWI_additional_military_facilities
	complete_national_focus = SWI_strengthen_the_fdfa
	complete_national_focus = SWI_strengthen_the_fdha
	
	complete_national_focus = SWI_the_cern_project
	complete_national_focus = SWI_the_large_hadron_collider
	complete_national_focus = SWI_geneva_industrial_project
	
	complete_national_focus = SWI_military_expansion
}

2015.9.18 = {

	set_politics = {
		parties = {
			nationalist = {
				popularity = 1.2
			}
			reactionary = {
				popularity = 29.4
			}
			conservative = {
				popularity = 6
			}
			market_liberal = {
				popularity = 16.4
			}
			social_liberal = {
				popularity = 11.6
			}
			social_democrat = {
				popularity = 18.8
			}
			progressive = {
				popularity = 11.7
			}
			democratic_socialist = {
				popularity = 1
			}
		}

		ruling_party = market_liberal
		last_election = "2015.9.18"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Albert Rosti"
		picture = "Albert_Rosti.dds"
		ideology = counter_progressive_democrat
	}
}