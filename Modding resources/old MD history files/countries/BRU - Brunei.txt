﻿capital = 834

oob = "BRU_2000"

set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_malay

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		monarchist = {
			popularity = 57
		}
		islamist = {
			popularity = 19
		}
		conservative = {
			popularity = 18
		}
		democratic_socialist = {
			popularity = 6
		}
	}
	
	ruling_party = monarchist
	last_election = "1967.10.4"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Sultan Hassanal Bolkiah"
	picture = "Hassanal_Bolkiah.dds"
	ideology = absolute_monarchist
}

2017.1.1 = {

oob = "BRU_2017"

#starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	infantry_weapons = 1
	
	combat_eng_equipment = 1

	command_control_equipment = 1

	Rec_tank_0 = 1

	ENG_MBT_1 = 1

	util_vehicle_equipment_0 = 1

	Early_APC = 1

	APC_1 = 1


	gw_artillery = 1

	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1

}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_8
	sunni
		stagnation
		defence_03
	edu_02
	health_03
	bureau_04
	police_05
	volunteer_army
	volunteer_women
	fossil_fuel_industry
	international_bankers
	The_Ulema
	hybrid
}
set_country_flag = gdp_8
set_country_flag = TPP_Signatory
set_country_flag = enthusiastic_fossil_fuel_industry
set_country_flag = positive_international_bankers
set_country_flag = positive_The_Ulema

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 3
		}
		
		communism = {
			popularity = 70
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 17
		}
	}
	
	ruling_party = communism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Hassanal Bolkiah"
	desc = ""
	picture = "BRU_Hassanal_Bolkiah.dds"
	expire = "2065.1.1"
	ideology = Autocracy
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Sultan Hassanal Bolkiah"
	picture = "Portrait_Hassanal_Bolkiah.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Sultan Al-Muhtadee Billah"
	picture = "Portrait_Al_Muhtadee_Billah.dds"
	traits = { offensive_doctrine }
	skill = 3
}

create_field_marshal = {
	name = "Mohammad Tawih Bin Abdullah"
	picture = "Portrait_Mohammed_Tawih_Bin_Abdullah.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Shahril Anwar Bin Haji Ma`awiah"
	picture = "Portrait_Shahril.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_corps_commander = {
	name = "Aminan Bin Pengiran Haji Mahmud"
	picture = "Portrait_Pengiran.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Haji Hamzah Bin Haji Sahat"
	picture = "Portrait_Hamzah.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Azmali Bin Pengiran Haji Mohd Salleh"
	picture = "Portrait_Azmali.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Damit Bin Haji Bakar"
	picture = "Portrait_Damit.dds"
	traits = { trickster }
	skill = 1
}

create_navy_leader = {
	name = "Norazmi Bin Pengiran Haji Muhammad"
	picture = "Portrait_Haji_Muhammad.dds"
	traits = { superior_tactician }
	skill = 2
}

}