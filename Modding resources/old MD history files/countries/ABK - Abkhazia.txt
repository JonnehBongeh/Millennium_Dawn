﻿capital = 796

oob = "ABK_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_russian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1


	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	extensive_conscription
	partially_recognized_state
}

set_politics = {

	parties = {
		social_democrat = { popularity = 67 }
		conservative = { popularity = 23 }
		reactionary = { popularity = 6 }	
		nationalist = { popularity = 2 }
		progressive = { popularity = 2 }
	}
	
	ruling_party = social_democrat
	last_election = "1999.10.3"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Vladislav Ardzinba"
	picture = "Vladislav_Ardzinba.dds"
	ideology = social_democrat_ideology
}

create_field_marshal = {
	name = "Vladimir Arshba"
	picture = "generals/Vladimir_Arshba.dds"
	skill = 1
}

create_corps_commander = {	#passed away November 26, 2008
	name = "Sultan Sosnaliev"
	picture = "generals/Sultan_Sosnaliyev.dds"
	skill = 1
}

2008.1.1 = {
	create_field_marshal = {
		name = "Anatoly Khrulyov"
		picture = "generals/Anatoly_Khrulyov.dds"
		skill = 1
	}
}

2015.1.1 = {
	set_politics = {
		parties = {
			social_democrat = { popularity = 49 }
			conservative = { popularity = 35 }
			reactionary = { popularity = 9 }
			communist = { popularity = 4 }
			nationalist = { popularity = 1 }
			social_liberal = { popularity = 1 }
			progressive = { popularity = 1 }
		}
		ruling_party = social_democrat
		elections_allowed = yes
		last_election = "2014.8.24"
	}
	
	create_country_leader = {
		name = "Raul Khajimba"
		picture = "Raul_Khajimba.dds"
		ideology = social_democrat_ideology
	}

}
2017.1.1 = {

oob = "ABK_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
}

add_ideas = {
	pop_050
	systematic_corruption
	orthodox_christian
	gdp_3
	defence_06
	edu_02
	health_03
	social_02
	bureau_03
	police_02
	draft_army
	volunteer_women
	farmers
	oligarchs
	small_medium_business_owners
	civil_law
}
set_country_flag = gdp_3
set_country_flag = negative_small_medium_business_owners

	set_politics = {

	parties = {
		democratic = { 
			popularity = 3
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 85
			#banned = no #default is no
		}
		neutrality = { 
			popularity = 12
		}
	}
	
	ruling_party = communism
	last_election = "2014.8.24"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Raul Khajimba"
	desc = ""
	picture = "ABK_Raul_Khajimba.dds"
	expire = "2050.1.1"
	ideology = Conservative
	traits = {
		#
		}
	}

}