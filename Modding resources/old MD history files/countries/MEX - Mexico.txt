﻿capital = 277

oob = "MEX_2000"

set_convoys = 730
set_stability = 0.5

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1

	corvette1 = 1
	corvette2 = 2
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	limited_conscription
}

set_politics = {

	parties = {
		market_liberal = {
			popularity = 36
		}
		social_democrat = {
			popularity = 30
		}
		conservative = {
			popularity = 30
		}
		progressive = {
			popularity = 2
		}
		monarchist = { 
			popularity = 1
		}
		nationalist = { 
			popularity = 1
		}
	}
	
	ruling_party = market_liberal
	last_election = "1994.8.21"
	election_frequency = 72
	elections_allowed = yes
}

add_opinion_modifier = {
	target = CAN
	modifier = NAFTA
}

add_opinion_modifier = {
	target = USA
	modifier = NAFTA
}

create_country_leader = {
	name = "Maximilian von Götzen-Iturbide"
	ideology = absolute_monarchist
	picture = "Maximilian.dds"
}
create_country_leader = {
	name = "Ernesto Zedillo"
	ideology = libertarian
	picture = "Ernesto_Zedillo.dds"
}
create_country_leader = {
	name = "Diego Fernandez de Cevallos"
	ideology = right_wing_conservative
	picture = "Diego_Fernandez_de_Cevallos.dds"
}
create_country_leader = {
	name = "Cuauhtemoc Cardenas Solorzano"
	ideology = social_democrat_ideology
	picture = "Cuauhtemoc_Cardenas_Solorzano.dds"
}
create_country_leader = {
	name = "Cecilia Soto Gonzalez"
	ideology = progressive_ideology
	picture = "Cecilia_Soto_Gonzalez.dds"
}
create_corps_commander = {
	name = "Carlos A. R. Munguia"
	picture = "generals/Carlos_AR_Munguia.dds"
	skill = 1
}
create_corps_commander = {
	name = "Daniel Velasco Ramirez"
	picture = "generals/Daniel_Velasco_Ramirez.dds"
	skill = 1
}
create_corps_commander = {
	name = "Enrique Cervantes Aguirre"
	picture = "generals/Enrique_Cervantes_Aguirre.dds"
	skill = 1
}
create_corps_commander = {
	name = "Gerardo Clemente Vega Garcia"
	picture = "generals/Gerardo_CV_Garcia.dds"
	skill = 1
}
create_corps_commander = {
	name = "Gilberto Hernandez Andreu"
	picture = "generals/Gilberto_Hernandez_Andreu.dds"
	skill = 1
}
create_corps_commander = {
	name = "Guillermo Galvan"
	picture = "generals/Guillermo_Galvan.dds"
	skill = 1
}
create_corps_commander = {
	name = "Juan Rios Cantu"
	picture = "generals/Juan_Rios_Cantu.dds"
	skill = 1
}
create_corps_commander = {
	name = "Roble Arturo Granados Gallardo"
	picture = "generals/Roble_AG_Gallardo.dds"
	skill = 1
}
create_corps_commander = {
	name = "Salvador Cienfuegos Zepeda"
	picture = "generals/Salvador_Cienfuegos_Zepeda.dds"
	skill = 1
}
create_corps_commander = {
	name = "Tomas Angeles Dauahare"
	picture = "generals/Tomas_Angeles_Dauahare.dds"
	skill = 1
}

create_navy_leader = {
	name = "Jose Ramon Lorenzo"
	picture = "admirals/Jose_Ramon_Lorenzo_Franco.dds"
	skill = 1
}
create_navy_leader = {
	name = "Marco Antonio Peyrot Gonzalez"
	picture = "admirals/Marco_Antonio_Peyrot_Gonzalez.dds"
	skill = 1
}
create_navy_leader = {
	name = "Mariano Francisco Saynez Mendoza"
	picture = "admirals/Mariano_Francisco_Saynez_Mendoza.dds"
	skill = 1
}
create_navy_leader = {
	name = "Vidal Francisco Soberon Sanz"
	picture = "admirals/Vidal_Francisco_Soberon_Sanz.dds"
	skill = 1
}

2000.7.2 = {
	set_politics = {
		ruling_party = conservative
		last_election = "2000.6.2"
		election_frequency = 72
		elections_allowed = yes
	}
    create_country_leader = {
		name = "Vicente Fox"
		picture = "Vicente_Fox.dds"
		expire = "2050.1.1"
		ideology = gaullist
	}
	create_country_leader = {
		name = "Cuauhtemoc Cardenas Solorzano"
		ideology = social_democrat_ideology
		picture = "Cuauhtemoc_Cardenas_Solorzano.dds"
		expire = "2050.7.2"
	}
	create_country_leader = {
		name = "Francisco Labastida Ochoa"
		picture = "Francisco_Labastida_Ochoa.dds"
		expire = "2050.1.1"
		ideology = libertarian
	}
}

2006.7.2 = {
	set_politics = {
		ruling_party = conservative
		last_election = "2006.6.2"
		election_frequency = 72
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Felipe Calderón"
		picture = "Felipe_Calderon.dds"
		ideology = gaullist
	}
	create_country_leader = {
		name = "Roberto Madrazo"
		ideology = social_democrat_ideology
		picture = "Roberto_Madrazo.dds"
	}
	create_country_leader = {
		name = "Andres Manuel Lopez Obrador"
		picture = "Andres_Manuel_Lopez_Obrador.dds"
		ideology = liberalist
	}
}

2012.7.2 = {
	set_party_name = {
		ideology = democratic_socialist
		long_name = MEX_democratic_socialist_party_NA_long
		name = MEX_democratic_socialist_party_NA
	}
	set_politics = {
		ruling_party = market_liberal
		last_election = "2012.6.2"
		election_frequency = 72
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Josefina Vasquez Mota"
		ideology = right_wing_conservative
		picture = "Josefina_Vasquez_Mota.dds"
	}
	create_country_leader = {
		name = "Andres Manuel Lopez Obrador"
		picture = "Andres_Manuel_Lopez_Obrador.dds"
		ideology = liberalist
	}
}

2016.1.1 = {
	set_politics = {

		parties = {
			market_liberal = {
				popularity = 32
			}
			social_liberal = {
				popularity = 31
			}
			conservative = {
				popularity = 29
			}
			reactionary = {
				popularity = 3
			}
			nationalist = { 
				popularity = 4
			}
			monarchist = { 
				popularity = 1
			}   
		}
		
		ruling_party = market_liberal
		last_election = "2012.2.1"
		election_frequency = 72
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Enrique Peña Nieto"
		ideology = libertarian
		picture = "Enrique_Pena_Nieto.dds"
	}
}