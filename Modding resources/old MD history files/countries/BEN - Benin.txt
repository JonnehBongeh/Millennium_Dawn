﻿capital = 769

oob = "BEN_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 10
		}
		social_liberal = {
			popularity = 80
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = social_liberal
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Mathieu Kerekou"
	picture = "Mathieu_Kerekou.dds"
	ideology = liberalist	
}

2006.4.6 = {
	create_country_leader = {
		name = "Thomas Boni Yayi"
		picture = "Thomas_Boni_Yayi.dds"
		ideology = liberalist
	}
}
	
2016.1.1 = {
	create_country_leader = {
		name = "Patrice Talon"
		picture = "Thomas_Patrice_Talon.dds"
		ideology = liberalist
	}
}
2017.1.1 = {

oob = "BEN_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1

	landing_craft = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_1
	    fast_growth
		defence_01
	edu_03
	health_01
	social_01
	bureau_03
	police_02
	draft_army
	volunteer_women
	international_bankers
	farmers
	small_medium_business_owners
	civil_law
	}
set_country_flag = gdp_1
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers
set_country_flag = negative_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots

	set_politics = {
	parties = {
		democratic = { 
			popularity = 2
		}
		fascism = {
			popularity = 4
		}
		
		communism = {
			popularity = 28
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 66
	}
	}
	
	ruling_party = neutrality
	last_election = "2016.3.20"
	election_frequency = 60
	elections_allowed = yes
	
}
create_country_leader = {
	name = "Thomas Boni Yayi"
	desc = ""
	picture = "BEN_Thomas_Boni_Yayi.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Patrice Talon"
	desc = ""
	picture = "BEN_patrice_talon.dds"
	expire = "2065.1.1"
	ideology = neutral_social
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Laurent Amoussou"
	picture = "Portrait_Laurent_Amoussou.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "O. Laleyle"
	picture = "Portrait_O_Laleyle.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Albert Badou"
	picture = "Portrait_Albert_Badou.dds"
	traits = {  }
	skill = 1
}

}