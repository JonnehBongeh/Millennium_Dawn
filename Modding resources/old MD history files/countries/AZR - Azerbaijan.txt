﻿capital = 229

oob = "AZB_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_azerbaijani

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	limited_conscription
}

set_country_flag = dominant_religion_islam
set_country_flag = shia_islam
give_military_access = TUR

add_opinion_modifier = { target = TUR modifier = large_increase }
add_opinion_modifier = { target = ARM modifier = rival }
add_opinion_modifier = { target = NGK modifier = rival }

set_politics = {

	parties = {
		islamist = {
			popularity = 10
		}
		nationalist = {
			popularity = 10
		}
		reactionary = {
			popularity = 30
		}
		conservative = {
			popularity = 20
		}
		market_liberal = {
			popularity = 2
		}
		social_liberal = {
			popularity = 10
		}
		social_democrat = {
			popularity = 2
		}
		progressive = {
			popularity = 2
		}
		democratic_socialist = {
			popularity = 2
		}
		communist = {
			popularity = 12
		}
	}
	
	ruling_party = reactionary
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Heydar Aliyev"
	picture = "Heydar_Aliyev.dds"
	ideology = oligarchist
}

add_namespace = {
	name = "azr_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Zakir Hasanov"
	picture = "generals/Zakir_Hasanov.dds"
	traits = { old_guard organisational_leader }
	skill = 1
}

create_corps_commander = {
	name = "Najmeddin Sadikov"
	picture = "generals/Najmeddin_Sadikov.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Kerim Veliyev"
	picture = "generals/Kerim_Veliyev.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Fuad Mammadov"
	picture = "generals/Fuad_Mammadov.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Ramiz Tairov"
	picture = "generals/Ramiz_Tairov.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Shahin Mammadov"
	picture = "generals/Shahin_Mammedov.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Elchin Guliyev"
	picture = "generals/Elchin_Gulyev.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Vagif Akhundov"
	picture = "generals/Vagif_Akhundov.dds"
	traits = { trickster }
	skill = 1
}

create_navy_leader = {
	name = "Shahin Sultanov"
	picture = "admirals/Shahin_Sultanov.dds"
	traits = { blockade_runner }
	skill = 1
}

2016.1.1 = {
	create_country_leader = {
		name = "Ilham Aliyev"
		picture = "Ilham_Aliyev.dds"
		ideology = oligarchist
	}
}
2017.1.1 = {

oob = "AZE_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#AK-74M Licensed
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	#Guyrza and licensed Marauder
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	
	#For templates
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1 
	MBT_1 = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	rampant_corruption
	gdp_3
	shia
	rentier_state
    export_economy
	    recession
		defence_05
	edu_02
	health_01
	social_03
	bureau_02
	police_05
	draft_army
	volunteer_women
	fossil_fuel_industry
	international_bankers
	The_Ulema
	civil_law
	}
set_country_flag = gdp_3
set_country_flag = Major_Importer_RUS_Arms

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 20
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 35
		}
		
		neutrality = {
			popularity = 40
		}
		
		nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = neutrality
	last_election = "2013.10.9"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Jamil Hasanli"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "jamil_hasanli.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Iqbal Aghazade"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "iqbal_aghazade.dds"
	expire = "2060.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Gudrat Hasanguliyev"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "gudrat_hasanguliyev.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ilham Aliyev"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "AZE_Ilham_Aliyev.dds"
	expire = "2065.1.1"
	ideology = Neutral_Autocracy
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Zakir Hasanov"
	picture = "Portrait_Zakir_Hasanov.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_corps_commander = {
	name = "Najmeddin Sadikov"
	picture = "Portrait_Najmeddin_Sadikov.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Kerim Veliyev"
	picture = "Portrait_Kerim_Veliyev.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Fuad Mammadov"
	picture = "Portrait_Fuad_Mammadov.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Ramiz Tairov"
	picture = "Portrait_Ramiz_Tairov.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Shahin Mammadov"
	picture = "Portrait_Shahin_Mammedov.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Elchin Guliyev"
	picture = "Portrait_Elchin_Gulyev.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Vagif Akhundov"
	picture = "Portrait_Vagif_Akhundov.dds"
	traits = { trickster }
	skill = 3
}

create_navy_leader = {
	name = "Shahin Sultanov"
	picture = "Portrait_Shahin_Sultanov.dds"
	traits = { blockade_runner }
	skill = 2
}
}