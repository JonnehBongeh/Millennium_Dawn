﻿capital = 670

oob = "LAO_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_lao

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	limited_conscription
}

set_politics = {

	parties = {
		
		nationalist = {
			popularity = 7
		}
		social_democrat = {
			popularity = 2
		}
		conservative = {
			popularity = 8
		}
		democratic_socialist = {
			popularity = 3
		}
		social_liberal = {
			popularity = 5
		}
		communist = {
			popularity = 75
		}
	}
	
	ruling_party = communist
	last_election = "1992.11.24"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Khamtay Siphandone"
	picture = "Khamtay_Siphandone.dds"
	ideology = maoist
}
create_corps_commander = {
	name = "Douangchay Phichit"
	picture = "generals/Douangchay_Phichit.dds"
	skill = 1
}


2016.1.22 = {
	create_country_leader = {
		name = "Bounnhang Vorachith"
		picture = "Bounnhang_Vorachith.dds"
		ideology = maoist
	}
}