﻿add_namespace = test

country_event = {
	id = test.0
	title = test.0.t
	desc = test.0.d
	
	picture = GFX_news_event_war
	major = yes
	
	is_triggered_only = yes
	
	trigger = {
		SAU = { is_bad_salafist = yes }
	}

	option = {
		name = war.0.a
		log = "[GetDateText]: [This.GetName]: war.0.a executed"
		if = {
			limit = { SAU = { is_bad_salafist = yes } }
			add_political_power = 50
		}
	}
}

country_event = {

	id = test.1
	title = test.1.t
	desc = test.1.d
	
	is_triggered_only = yes
	
	option = {
		name = "print local vars"
		log = "[GetDateText]: [This.GetName]: test.1.a executed"
		print_variables = {
			file = log_file
			append = yes
			print_global = no
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = "print global vars"
		log = "[GetDateText]: [This.GetName]: test.1.b executed"
		print_variables = {
			file = log_file
			append = yes
			print_global = yes
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = "print region arrays"
		log = "[GetDateText]: [This.GetName]: test.1.c executed"
		set_power_ranking_ideas_for_every_country = yes
		print_region_arrays = yes
		ai_chance = { factor = 100 }
	}
}

country_event = {

	id = test.2
	title = test.2.t
	desc = test.2.d
	
	is_triggered_only = yes
	
	option = {
		name = "Puppet Germany"
		log = "[GetDateText]: [This.GetName]: test.2.a executed"
		puppet = GER
		GER = {
			effect_puppet_match_ruling_party_to_overlord = yes
		}
	}
	option = {
		name = "Release Germany"
		log = "[GetDateText]: [This.GetName]: test.2.b executed"
		end_puppet = GER
	}
}

country_event = {

	id = test.3
	title = test.3.t
	desc = test.3.d
	
	is_triggered_only = yes
	
	option = {
		name = "Load 2017 Syrian focus"
		log = "[GetDateText]: [This.GetName]: test.3.a executed"
		load_focus_tree = Syria_Focus_2017
	}
}

country_event = {

	id = test.4
	title = test.4.t
	desc = test.4.d
	
	is_triggered_only = yes
	
	option = {
		name = "Load 2000 Syrian focus"
		log = "[GetDateText]: [This.GetName]: test.4.a executed"
		load_focus_tree = Syria_2000
	}
}
