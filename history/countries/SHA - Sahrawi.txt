﻿2000.1.1 = {
	capital = 372
	oob = "SHA_2000"
	set_convoys = 20
	add_ideas = {
		#pop_050
		unrestrained_corruption
		gdp_3
		sufi_islam
		defence_09
		edu_02
		health_01
		social_01
		bureau_04
		police_03
		volunteer_army
		volunteer_women
		The_Ulema
		small_medium_business_owners
		farmers
		#hybrid
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 75 }
	add_to_array = { influence_array = ALG.id }
	add_to_array = { influence_array_val = 100 }
	add_to_array = { influence_array = MAU.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = LBA.id }
	add_to_array = { influence_array_val = 45 }
	add_to_array = { influence_array = CUB.id }
	add_to_array = { influence_array_val = 15 }
	startup_influence = yes
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}
	
	set_popularities = {
		democratic = 100.0
		communism = 0.0
		fascism = 0.0
		neutrality = 0.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "2000.1.1"
		election_frequency = 60
		elections_allowed = yes
	}
	
	start_politics_input = yes

	### Party Popularities
	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 1.0 } #conservatism
	set_variable = { party_pop_array^2 = 0 } #liberalism
	set_variable = { party_pop_array^3 = 0 } #socialism
	set_variable = { party_pop_array^4 = 0 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist

	### Ruling Party
	add_to_array = { ruling_party = 1 }

	startup_politics = yes

	create_country_leader = {
		name = "Mohamed Abdelaziz"
		picture = "Mohamed_Abdelaziz.dds"
		ideology = conservatism
		traits = {
			western_conservatism
		}
	}
}

2017.1.1 = {
	capital = 372
	oob = "SHA_2017"
	set_convoys = 20

	add_manpower = 5000

	add_ideas = {
		#pop_050
		unrestrained_corruption
		gdp_3
		sufi_islam
		defence_09
		edu_02
		health_01
		social_01
		bureau_04
		police_03
		volunteer_army
		volunteer_women
		The_Ulema
		small_medium_business_owners
		farmers
		#hybrid
	}
	#set_country_flag = gdp_3
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 75 }
	add_to_array = { influence_array = ALG.id }
	add_to_array = { influence_array_val = 120 }
	add_to_array = { influence_array = MAU.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = LBA.id }
	add_to_array = { influence_array_val = 0 }
	add_to_array = { influence_array = CUB.id }
	add_to_array = { influence_array_val = 15 }
	startup_influence = yes

	#Nat focus
	
	
		
	add_opinion_modifier = { target = MOR modifier = SHA_MOR_Occupation }
	add_opinion_modifier = { target = ALG modifier = SHA_Support_Our_Independence }

	set_popularities = {
		democratic = 0.0
		communism = 0.0
		fascism = 0.0
		neutrality = 100.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = neutrality
		last_election = "2014.4.17"
		election_frequency = 60
		elections_allowed = yes
	}

	start_politics_input = yes

	### Party Popularities
	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0 } #conservatism
	set_variable = { party_pop_array^2 = 0 } #liberalism
	set_variable = { party_pop_array^3 = 0 } #socialism
	set_variable = { party_pop_array^4 = 0 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 1.0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist

	### Ruling Party
	add_to_array = { ruling_party = 18 }

	startup_politics = yes

	create_country_leader = {
		name = "Brahim Ghali"
		picture = "SHA_Ibrahim_Ghali.dds"
		ideology = neutral_Social
		traits = {
			neutrality_neutral_Social
		}
	}
}