ideas = {

	country = {
			
		subject_hates = {
				
			allowed = {
				is_subject = yes
			}

			modifier = {
				autonomy_gain_global_factor = 1.0
			}
		}
			
		subject_negative = {
				
			allowed = {
				is_subject = yes
			}

			modifier = {
				autonomy_gain_global_factor = 0.5
			}
		}
			
		subject_neutral = {
				
			allowed = {
				is_subject = yes
			}

			modifier = {
				autonomy_gain_global_factor = 0.0
			}
		}
			
		subject_positive = {
				
			allowed = {
				is_subject = yes
			}

			modifier = {
				autonomy_gain_global_factor = -0.5
			}
		}
			
		subject_loves = {
				
			allowed = {
				is_subject = yes
			}

			modifier = {
				autonomy_gain_global_factor = -1.0
			}
		}
	}

}