defined_text = {
   name = GetNukeStatus

	text = {
	   trigger = { ROOT = { num_of_nukes > 0 } }
	   localization_key = has_nukes_yes_TT
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = has_nukes_no_TT
	}
}

defined_text = {
	name = GerWarStatus
	
	text = {
		trigger = { owner = { has_war_with = ROOT } }
		localization_key = has_war_TT
	}
	text = {
		trigger = { owner = { NOT = { has_war_with = ROOT } } }
		localization_key = not_has_war_TT
	}
}

defined_text = {
	name = GetBomberStatus
	
	text = {
		trigger = {
			ROOT = {
				has_deployed_air_force_size = {
					size > 0
					type = strategic_bomber
				}
			}
		}
		localization_key = has_bombers_TT
	}
	text = {
		trigger = {
			ROOT = {
				has_deployed_air_force_size = {
					size < 1
					type = strategic_bomber
				}
			}
		}
		localization_key = not_has_bombers_TT
	}
}