set_leader_UNI = {

	if = { limit = { has_country_flag = set_Western_Autocracy }

		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } }
			set_variable = { Western_Autocracy_leader = 1 }
			kill_country_leader = yes

			create_country_leader = {
				name = "Jonas Savimbi"
				picture = "Jonas_Savimbi.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
}